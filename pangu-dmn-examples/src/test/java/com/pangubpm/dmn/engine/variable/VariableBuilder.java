/**
 * Copyright (c) 2019-2023 A1Bpm All rights reserved.
 * <p>
 * http://www.a1bpm.com
 * <p>
 * 版权所有，侵权必究！
 */
package com.pangubpm.dmn.engine.variable;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.pangubpm.bpm.engine.variable.VariableMap;
import com.pangubpm.bpm.engine.variable.Variables;
import com.pangubpm.bpm.engine.variable.impl.VariableMapImpl;
import com.pangubpm.bpm.engine.variable.value.BooleanValue;
import com.pangubpm.bpm.engine.variable.value.ObjectValue;
import com.pangubpm.bpm.engine.variable.value.PrimitiveValue;
import com.pangubpm.bpm.engine.variable.value.TypedValue;

public class VariableBuilder {

  /**
   * 构造变量
   */
  @Test
  public void testNoResult() {
    VariableMap variableMap=new VariableMapImpl();
    variableMap.putValue("string","testString");
    assertThat(variableMap).isNotEmpty();
    variableMap.putValueTyped("int", Variables.integerValue(10));
    variableMap.putValueTyped("int",Variables.booleanValue(true));
    variableMap.putValueTyped("number",Variables.numberValue(10));
    variableMap.putValueTyped("number",Variables.longValue(10L));
    Map<String, Object> stringObjectMap = ((VariableMapImpl) variableMap).asValueMap();
    assertThat(stringObjectMap).hasSize(3);
  }
  /**
   * 变量类型
   */
  @Test
  public void untypedValue() {
    TypedValue typedValue = Variables.untypedValue(Variables.booleanValue(true));
    assertThat(typedValue).isInstanceOf(BooleanValue.class);
     typedValue = Variables.untypedValue(Variables.integerValue(10));
    assertThat(typedValue).isInstanceOf(PrimitiveValue.class);
  }
  /**
   * map集合转换为VariableMap
   */
  @Test
  public void fromMap() {
    Map<String, Object> map=new HashMap<String, Object>();
    map.put("int", Variables.integerValue(10));
    map.put("boolean",Variables.booleanValue(true));
    map.put("number",Variables.numberValue(10));
    VariableMap variableMap = Variables.fromMap(map);
    assertThat(variableMap).hasSize(3);
  }
  /**
   * objectValue
   */
  @Test
  public void objectValue() {
    CustomerData customerData=new CustomerData();
    customerData.setAge(18);
    customerData.setName("盘古DMN");
    ObjectValue objectValue = Variables.objectValue(customerData)
      .serializationDataFormat(Variables.SerializationDataFormats.JSON)
      .create();
  }
}
